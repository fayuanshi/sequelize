/*
 * @Author: FayuanShi
 * @Date: 2021-04-20 20:36:21
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-04-20 23:20:37
 * @Description: file content
 */
module.exports = app=>{
    const express = require('express')
    const router = express.Router({
        mergeParams:true
    })

    router.post('/',async (req,res)=>{
        const jane = await require('../models/index').create(req.body);
        res.send(jane)
    }) 

    router.put('/',async (req,res)=>{
        const jane = await require('../models/index').update(
            {name:req.body.name},
            {
             where:{
                 id:req.body.id
             }
            }
        );
        res.send()
    }) 

    router.delete('/',async (req,res)=>{
        await require('../models/index').destroy({
            where:{
                id:req.body.id
            }
        })
    })

    router.get('/:id',async (req,res)=>{
        
        const user = await require('../models/index').findAll({
            id:req.params.id
        })
        res.send(user)
    })
    
    app.use('/user',router)
}