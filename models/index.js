/*
 * @Author: FayuanShi
 * @Date: 2021-04-20 20:31:51
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-04-20 22:43:19
 * @Description: file content
 */
const {Sequelize,DataTypes} = require('sequelize');
const sequelize = require('../utils/db')

const User = sequelize.define("user", {
    name: DataTypes.TEXT,
    favoriteColor: {
      type: DataTypes.TEXT,
      defaultValue: 'green'
    },
    age: DataTypes.INTEGER,
    cash: DataTypes.INTEGER
  },
  { define: { freezeTableName: true } }
  );
  
  (async () => {
    await sequelize.sync();
    // 这里是代码
  })();

  module.exports = User
  
