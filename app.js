/*
 * @Author: FayuanShi
 * @Date: 2021-04-20 20:10:49
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-04-20 22:20:42
 * @Description: file content
 */


const express = require('express')
const app = express()
const cors = require('cors')

require('./utils/db.js')

app.use(express.json())

app.use(cors())
require('./controllers/UserController')(app)


app.listen("3333",()=>{
    console.log("run as 3333")
})